---
hide:
  - footer
  - toc
---

# UCL PBa IT Sikkerhed

På dette website finder du ugeplaner, opgaver, dokumenter og links til brug i faget 

## Systemsikkerhed forår 2023

Den studerende kan udføre, udvælge, anvende, og implementere praktiske tiltag
til sikring af firmaets udstyr og har viden og færdigheder der supportere dette.

### Studieordning

Studieordningen er i 2 dele, en national del og en institutionel del.  

Begge dele kan findes i studiedokumenter på ucl.dk [https://www.ucl.dk/studiedokumenter/it-sikkerhed](https://www.ucl.dk/studiedokumenter/it-sikkerhed)

## Læringsmål fra studie ordningen

### Viden

**_Den studerende har viden om_**

- Generelle governance principper / sikkerhedsprocedurer
- Væsentlige forensic processer
- Relevante it-trusler
- Relevante sikkerhedsprincipper til systemsikkerhed
- OS roller ift. sikkerhedsovervejelser
- Sikkerhedsadministration i DBMS.

### Færdigheder

**_Den studerende kan_**

- Udnytte modforanstaltninger til sikring af systemer
- Følge et benchmark til at sikre opsætning af enhederne
- Implementere systematisk logning og monitering af enheder
- Analysere logs for incidents og følge et revisionsspor
- Kan genoprette systemer efter en hændelse.

### Kompetencer

**_Den studerende kan_**

- håndtere enheder på command line-niveau
- håndtere værktøjer til at identificere og fjerne/afbøde forskellige typer af endpoint trusler
- Håndtere udvælgelse, anvendelse og implementering af praktiske mekanismer til at forhindre, detektere og reagere over for specifikke it-sikkerhedsmæssige hændelser.
- håndtere relevante krypteringstiltag

<!-- ![Image light theme](images/UCL_horisontal_logo_UK_rgb.png#only-light){ width="300", align=right }
![Image dark theme](images/UCL_horisontal_logo_UK_neg_rgb.png#only-dark){ width="300", align=right } -->

