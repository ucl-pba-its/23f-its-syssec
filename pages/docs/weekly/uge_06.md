---
Week: 06
Content: Introduktion til faget og introduktion til Linux.
Material: 
Initials: MESN
# hide:
#  - footer
---

# Uge 06 - *Introduktion til faget*

## Emner

Ugens emner er:

- Introduktion til faget System sikkerhed
- Opsætning af ubuntu server

## Mål for ugen

I denne uge er målet at de studerende forstår hvad de forventes af dem
i faget, og hvad de overordnet læringsmål for faget er.

### Praktiske mål

- Alle studerende forstår fagets formål.
- Alle studerende kan tolke læringsmålene for faget.
- Alle studerende har en fungerende Ubuntu Server VM. 

### Læringsmål der arbejdes med i faget denne uge
_I denne uge arbejde vi ikke med konkrette læringsmål fra studieordningen, men forståelsen for dem_

## Skema

### Torsdag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 12:15  | Introduktion til dagen |
| 12:45  | Gruppe arbejde med forståelse for fagets læringsmål (Opgave 1) |
| 13:45  | Pause |
| 14:00  | Opgave 1 forsat - Udfyld padlet |
| 14:10  | Opsamling på læringsmål |
| 14:30  | Opsætning af ubuntu server VM |
| 15:20  | Status på opsætning af VM og opsamling på dagen |
| 15:30 | Fyraften               |

## Dagens øvelser  
[Ogave 1 - Fagets Læringsmål](https://ucl-pba-its.gitlab.io/exercises/system/1_fagets_l%C3%A6ringsm%C3%A5l/)



## Kommentarer

- ..
