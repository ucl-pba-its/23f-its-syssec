---
Week: 07
Content: Grundlæggende Linux
Material:
Initials: MESN
# hide:
#  - footer
---

# Uge 07 - *grundlæggende Linux*

## Emner

Ugens emner er:

- Introduktion til operativ systemer
- Grundlæggende Linux kommandoer

## Mål for ugen

Den studerende har grundlæggende forståelse for operativ systemer,
og kan navigere i Linux CLI.

### Praktiske mål

- Alle studerende har kendskab til de grundlæggende Linux kommandoer.
- Alle studerende kan navigere i Linux file struktur.
- Alle studerende kan oprette og  slette filer i Linux
- Alle studerende kan oprette og slette directories i Linux
- Alle studernede kan fortag en søg på en file eller mappe i Linux
- Alle studerende kan identificer en proces
- Alle studerende kan "dræbe en proces"

### Læringsmål der arbejdes med i faget denne uge

**Overordnede læringsmål fra studie ordningen:**


- **Kompetencer:** 
    - Den studerende kan håndtere enheder på command line-niveau

## Skema

### Mandag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 08:15  | Arbejde med opgave 2-8  |
| 15:30 | Fyraften               |

## Dagens øvelser  
[Ogave 2 - Operativ systemets formål](https://ucl-pba-its.gitlab.io/exercises/system/2_Opreativ_systemets_Form%C3%A5l/)  
[Ogave 3 - Navigation i LInux file systemet med bash](https://ucl-pba-its.gitlab.io/exercises/system/3_Navigation_i_Linux_filesystem_med_bash/)  
[Ogave 4 - Linux File struktur](https://ucl-pba-its.gitlab.io/exercises/system/4_Linux_file_struktur/)  
[Ogave 5 - Hjælp i Linux](https://ucl-pba-its.gitlab.io/exercises/system/5_Help_in_linux/)  
[Ogave 6 - Søgning i LInux file system](https://ucl-pba-its.gitlab.io/exercises/system/6_Searching_in_linux_file_system/)  
[Ogave 7 - Ændring og søgning i filer](https://ucl-pba-its.gitlab.io/exercises/system/7_changing_and_searching_in_files/)  
[Ogave 8 - Processer og services i Linux](https://ucl-pba-its.gitlab.io/exercises/system/8_Processes_and_services_in_Linux/)  

## Kommentarer
Jeg lægger syg, så i skal arbejde selvstændigt med opgave 2 - 8, vi følger op i uge 9
