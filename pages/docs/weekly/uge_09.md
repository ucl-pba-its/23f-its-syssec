---
Week: 09
Content: Bruger system i Linux
Material: xxx
Initials: MESN
# hide:
#  - footer
---

# Uge 09 - *Bruger systemer i Linux*

## Emner

Ugens emner er:

- Bruger systemer
- Bruger rettigheder
- Bruger systemer i Linux

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- Den studerende kan oprette, slette eller ændre en bruger i bash shell
- Den studernede kan ændre bruger rettighederne i bash shell
- Den studerende har forståelse for sårbarheder ift. bruger rettigheder

### Læringsmål der arbejdes med i faget denne uge

**Overordnede læringsmål fra studie ordningen:**

- **Viden:** 
- Relevante it-trusler
- Relevante sikkerhedsprincipper til systemsikkerhed
- OS roller ift. sikkerhedsovervejelser
- **Færdigheder:**
- Udnytte modforanstaltninger til sikring af systemer
- **Kompetencer:**
- Håndtere enheder på command line-niveau
- Håndtere udvælgelse, anvendelse og implementering af praktiske mekanismer til at forhindre it-sikkerhedsmæssige hændelser


## Skema

### Torsdag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 12:15  | Introduktion til dagen og opsamling på operativ systemer |
| 12:35 | gruppe øvelse med privilege of least & privilege escalation samt opsamling           |
| 13:15 | kort oplæg om bruger systemer           |
| 13:15 | Gruppe øvelser med brugersystemer og CIA modellen samt opsamling          |
| 13:45 | pause         |
| 14:00 | Øvelser med Linux bruger systemet       |
| 15:20 | Opsamling på dagen          |
| 15:30 | Fyraften               |

### Øvelser
- [Øvelse 9](https://ucl-pba-its.gitlab.io/exercises/system/9_User_accounts/)
- [Øvelse 10](https://ucl-pba-its.gitlab.io/exercises/system/10_User_permissions_in_linux/)
- [Øvelse 11](https://ucl-pba-its.gitlab.io/exercises/system/11_Special_Permissions/)

## Kommentarer

- ..
