---
Week: 10
Content: Loging
Initials: MESN
# hide:
#  - footer
---

# Uge 10 - *Loging*

## Emner

Ugens emner er:

- Loging

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- Den studerende kan redegøre for hvad der skal logges
- Den studerende har kendskab til Linux logging system

### Læringsmål der arbejdes med i faget denne uge

**Overordnede læringsmål fra studie ordningen:**

- **Viden:**
- Generelle governance principper
- Relevante  it-trusler
- Relevante sikkerhedsprincipper til systemsikkerhed
- **Færdigheder:** ..
- implementere systematisk logning og monitering af enheder (Påbegyndt)
- Analyser logs for incidents og følge et revisionsspor (Påbegyndt)
- **Kompetencer:** ..
- Håndtere udvælgelse af  praktiske mekanismer til  at dektektere it-sikkerhedsmæssige hændelser.

## Torsdag

### Mandag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 08:15 | Introduktion til dagen |
| 08:20 | Gennemgang af rettigheds øvelser fra sidst |
| 08:35 | System mål og sikkerheds mål|
| 09:45 | pause|
| 10:00 |Log management|
| 11:00 |Øvelser med Linux logging system|

### Dagens øvelser
[Opgave 12](https://ucl-pba-its.gitlab.io/exercises/system/12_Linux_system_og_auth_logs/)
