---
Week: 11
Content: logging
Initials: MESN
# hide:
#  - footer
---

# Uge 11 - Logging forsat

## Emner

Ugens emner er:

- Logging
- Logging i Linux

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- Den studerende kan redegøre for hvad der skal logges
- Den studerende har kendskab til Linux logging system

### Læringsmål der arbejdes med i faget denne uge

**Overordnede læringsmål fra studie ordningen:**

- **Viden:**
- Generelle governance principper
- Relevante  it-trusler
- Relevante sikkerhedsprincipper til systemsikkerhed
- **Færdigheder:** ..
- implementere systematisk logning og monitering af enheder (Påbegyndt)
- Analyser logs for incidents og følge et revisionsspor (Påbegyndt)
- **Kompetencer:** ..
- Håndtere udvælgelse af  praktiske mekanismer til  at dektektere it-sikkerhedsmæssige hændelser.

## Skema

### Torsdag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 12:15  | Introduktion til dagen |
| 12:30 | Øvelse: Udarbejde en log politik              |
| 13:00  | Opsamling på øvelse |
| 13:10  | Log management oplæg forsat |
| 13:15  | Øvelse: CIA & Logging |
| 13:20  | Opsamling på øvelse |
| 13:25  | Kort oplæg om trusler mod logs |
| 13:30  | Øvelse: Trusler mod logs |
| 13:45  | Pause |
| 14:00  | Opsamling på øvelse |
| 14:10  | Oplæg om log formatter |
| 14:20  | Linux øvelser |
| 15:20  | Opsamling på dagen |
| 15:30  | Dagen slut |

## Dagens opgave
[Opgave 12](https://ucl-pba-its.gitlab.io/exercises/system/12_Linux_system_og_auth_logs/)  
[Opgave 13](https://ucl-pba-its.gitlab.io/exercises/system/13_Linux_rsyslog/)  
[Opgave 14](https://ucl-pba-its.gitlab.io/exercises/system/14_Linux_rsyslog_rules/)  
[Opgave 15](https://ucl-pba-its.gitlab.io/exercises/system/15_Linux_log_rotate/)  
[Opgave 16](https://ucl-pba-its.gitlab.io/exercises/system/16_Linux_Disable_logging/)  
[Opgave 17](https://ucl-pba-its.gitlab.io/exercises/system/17_Linux_log_overblik/)  
[Opgave 18](https://ucl-pba-its.gitlab.io/exercises/system/18_Linux_application_logs/)  
  
## Kommentarer

- ..
