---
Week: 12
Content: Fysisk sikkerhed og Host-based firewall
Initials: MESN
# hide:
#  - footer
---

# Uge 12 - *Fysisk sikkerhed og Host-based firewall*

## Emner

Ugens emner er:

- Fysisk sikkerhed
- Host-based firewall.

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- En forståelse for at hændelser i den fysiske verden også kan udgører en it-sikkerheds trussel.
- At den studerende kan lave en grundlæggende opsætning af en firewall.
- At den studerende har forståelse for regel opsætning på en firewall.

### Læringsmål der arbejdes med i faget denne uge

**Overordnede læringsmål fra studie ordningen:**

- **Viden:**
  - Generelle sikkerhedprocedurer.
  - Relevante  it-trusler
- **Færdigheder:** ..
  - Udnytte modforanstaltninger til sikring af systemer
- **Kompetencer:** ..
  - Håndtere værktøjer til at fjerne forskellige typer af endpoint trusler.
  - Håndtere udvælgelse og anvend af praktiske til at forhindre it-sikkerhedsmæssige hændelser. 
## Skema

### Torsdag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 12:15 | Introduktion til dagen |
| 12:30 | Oplæg: Fysisk sikkerhed |
| 12:40 | Gruppe opgave|
| 12:50 | Opsamling på gruppe opgave|
| 13:00 | Oplæg: Fysisk sikkerhed forsat|
| 13:10 | Gruppe opgave|
| 13:20 | Opsamling på gruppe opgave|
| 13:30 | Oplæg: Fysisk sikkerhed forsat |
| 13:45 | Pause |
| 14:00 | Oplæg: Firewall |
| 14:30 | Linux iptables firewall øvelser|

## Opgaver
[Opgave 19](https://ucl-pba-its.gitlab.io/exercises/system/19_Linux_Installing_iptables/)  
[Opgave 20](https://ucl-pba-its.gitlab.io/exercises/system/20_Linux_Dissallowing_everything_iptables/)  
[Opgave 21](https://ucl-pba-its.gitlab.io/exercises/system/21_Linux_Allow_Self_established_connections/)  
[Opgave 22](https://ucl-pba-its.gitlab.io/exercises/system/22_Linux_Allowing_loopback/)  
[Opgave 23](https://ucl-pba-its.gitlab.io/exercises/system/23_Linux_Allowing_specific_ICMP_messages/)  
[Opgave 24](https://ucl-pba-its.gitlab.io/exercises/system/24_Linux_Allowing_SSH/)  
[Opgave 25](https://ucl-pba-its.gitlab.io/exercises/system/25_Linux_Allowing_Specific_IP/)  
