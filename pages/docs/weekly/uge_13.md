---
Week: 13
Content: Forenicis & audit.
Initials: MESN
# hide:
#  - footer
---

# Uge 13 - *Forenicis & audit*

## Emner

Ugens emner er:

- Efterforsknings proces.
- Linux audit system

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- At hver studerende har en grundlæggende forståelse for efterforsknings processen
- At hver studerende har prøvet at opsætte en audit regel for en enkelt file.
- At hver studerende har prøvet at opsætte en audit regel for et directory.
- At hver studerende har prøvet at opsætte en audit regel for et system kald.

### Læringsmål der arbejdes med i faget denne uge

**Overordnede læringsmål fra studie ordningen:**

- **Viden:**
- Den studerende har viden om væsentlige forensic processer
- Den studerende har viden om relevante it-trusler
- Relevante sikkerhedsprincipper til systemsikkerhed
- **Færdigheder:**
- Den studerende kan analysere logs for hændelser og følge et revisionsspor
- **Kompetencer:**
- Håndtere udvælgelse, anvendelse og implementering af praktiske mekanismer til at dektektere specifikke it-sikkerhedsmæssige hændelser.

## Skema

### Mandag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 08:15  | Introduktion til dagen |
| 08:45 | Oplæg om efterforsknings proces |
| 08:55 | Gruppe øvelse |
| 09:15 | Opsamling på gruppe øvelse |
| 09:25 | 5 minutters pause |
| 09:30 | Oplæg om efterforsknings proces forsat |
| 09:45 | 15 minutters pause |
| 10:00 | Audit gruppe øvelse |
| 10:20 | Audit gruppe øvelse opsamling |
| 10:30 | Audit oplæg |
| 11:00 | Opgaver |
| 11:30 | Tak for idag |

## Opgaver
[Opgave 26](https://ucl-pba-its.gitlab.io/exercises/system/26_Install_Auditd/)   
[Opgave 27](https://ucl-pba-its.gitlab.io/exercises/system/27_audit_file_for_changes/)  
[Opgave 28](https://ucl-pba-its.gitlab.io/exercises/system/28_audit_directory/)  
[Opgave 29](https://ucl-pba-its.gitlab.io/exercises/system/29_audit_system_calls/)  
[Opgave 30](https://ucl-pba-its.gitlab.io/exercises/system/30_create_hash_from_file/)  
