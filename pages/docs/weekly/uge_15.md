---
Week: 15
Content: Repetation og CIS
Initials: MESN
# hide:
#  - footer
---

# Uge 15 - *Repetation og CIS*

## Emner

Ugens emner er:

- Repetation og læringsmål
- CIS

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- Den studerende kan se overlappet mellem læringsmålene på fagene
- Den studerende forstår hvad sikkerheds benchmarks kan bruges til
- Den studerende forstår hvad CIS benchmarks kan bruges til
- Den studerende forstår samehængen mellem CIS benchmarks og CIS Controls
- Den studerende forstår hvad Mitre ATT&CK databasen kan anvendes til.

### Læringsmål der arbejdes med i faget denne uge

**Overordnede læringsmål fra studie ordningen:**
 
- **Færdigheder:** ..
  - Den studerende kan følge et benchmark til at sikre opsætning af enheder.

## Skema

### Torsdag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 08:15  | Introduktion til dagen |
| 0830   | Gennemgang af tidligere læringsmål og emner |
| 0845   | Gruppe opgave med læringsmål                |
| 0930   | Opsamling på Læringsmål opgave              |
| 0945   | Pause (HUSK FRISK LUFT!)  |
| 1000   | Oplæg om CIS Benchmarks & Controls  |
| 1020   | CIS Controller øvelse |
| 1040   | Opsamling på CIS  øvelse  |
| 1050   | Mitre ATT&CK oplæg  |
| 1100   | CIS Control & Benchmarks øvelse  |
| 1125   | Opsamling på dagen  |
| 11:30 | Fyraften               |
