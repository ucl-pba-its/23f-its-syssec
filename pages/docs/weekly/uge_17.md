---
Week: 17
Initials: MESN
# hide:
#  - footer
---

# Uge xx - *Trussels modellering og intro til SIEM*

## Emner

Ugens emner er:

- Trussels modellering
- Introduktion til SIEM systemer.

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- At hver studerende kan udfærdige en simple trussels modellering
- At hver studerende har grundlæggende forståelse for hvad et SIEM system er
- At hver studerende har implementeret simple alarmer med Wazuh
- At hver studerende har lavet simple filtering af sikkerheds hændelser i Wazuh

### Læringsmål der arbejdes med i faget denne uge

**Overordnede læringsmål fra studie ordningen:**

- **Viden:**
  - Viden om relevante it-trusler
- **Færdigheder:**
  - Kan implementerer systematisk logning og monitering af enheder
  - Kan analyser logs for hændelser og følge et revision spor
- **Kompetencer:**
  - Kan håndtere enheder på command line-niveau
 
### Tirsdag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 08:15 | Introduktion til dagen & opsamling  fra sidst |
| 08:30 | Oplæg: Trusselsmodellering       |
| 08:45 | Gruppe øvelse: Trusselsmodellering       |
| 09:10 | Opsamling på gruppe øvelse      |
| 09:20 | Oplæg: Intro til SIEM      |
| 09:30 | Gruppe øvelse: Konceptuelt design af SIEM system      |
| 09:50 | Pause       |
| 10:10 | Opsamling på gruppe øvelse      |
| 10:20 | Wazuh oplæg      |
| 10:30 | Wazuh øvelser      |
| 11:30 | Afslutning på dagen      |

## Øvelser
[Opgave 32](https://ucl-pba-its.gitlab.io/exercises/system/32_Wazuh_ops%C3%A6tning_af_agent/)  
[Opgave 33](https://ucl-pba-its.gitlab.io/exercises/system/33_Wazuh_Overv%C3%A5gning_af_filer/)  
[Opgave 34](https://ucl-pba-its.gitlab.io/exercises/system/34_Wazuh_detekter_fors%C3%B8g_p%C3%A5_Sql_inject_angreb/)  
[Opgave 35](https://ucl-pba-its.gitlab.io/exercises/system/35_Wazuh_detekter_fors%C3%B8g_p%C3%A5_shellshock_angreb/)  
[Opgave 36](https://ucl-pba-its.gitlab.io/exercises/system/36_Wazuh_detekter_fors%C3%B8g_p%C3%A5_ondsindet_kommandoer/)  
[Opgave 37](https://ucl-pba-its.gitlab.io/exercises/system/37_Wazuh_S%C3%A5rbarheds_Skanning/)  
