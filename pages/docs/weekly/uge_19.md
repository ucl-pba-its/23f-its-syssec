---
Week: 19
Initials: MESN
# hide:
#  - footer
---

# Uge 19 - *Overvågning med SIEM systemer*

## Emner

Ugens emner er:

- 7 lags modellen for detektering
- Kort introduktion til detekterings strategi
- Wazuh regler og dekoder.

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- Alle studerende har prøvet at arbejde med 7 lags modellen for detektering
- Alle studerende har implementeret en dekoder i Wazuh
- Alle studerende har implementeret en regel i Wazuh

### Læringsmål der arbejdes med i faget denne uge

**Overordnede læringsmål fra studie ordningen:**

- **Viden:** ..
- **Færdigheder:**
  - Kan implementerer systematisk logning og monitering af enheder
  - Kan analyser logs for hændelser og følge et revision spor
- **Kompetencer:**
  - Kan håndtere enheder på command line-niveau
  - Håndtere udvælgelse, anvendelse og implementering af praktiske mekanismer til at
    forhindre, detektere og reagere over for specifikke it-sikkerhedsmæssige hændelser.
  - håndtere værktøjer til at identificere og fjerne/afbøde forskellige typer af endpoint trusler

### Torsdag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 12:15  | Introduktion til dagen |
| 12:25 | Opsamling fra forrige gange               |
| 12:50 | 5 Minutters pause               |
| 12:55 | Detekteringens 7 Abstraktions lag               |
| 13:25 | Gruppe øvelse: Detekteringens 7 Abstraktions lag               |
| 13:55 | Pause               |
| 14:10 | Opsamling på gruppe øvelse               |
| 14:20 | Kort introduktion til detekterings strategi               |
| 14:30 | Kort introduktion Wazuh dekoder og regler               |
| 14:40 | Wazuh Øvelser               |

### Opgaver
[Opgave38](https://ucl-pba-its.gitlab.io/exercises/system/38_Wazuh_Tilpasset_dekterering_og_regel/)