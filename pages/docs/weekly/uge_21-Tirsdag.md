---
Week: xx
Content: Tilgår
Material: xxx
Initials: XXXX
# hide:
#  - footer
---

# Uge xx - *Titel*

## Emner

Ugens emner er:

- ..
- ..

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- ..
- ..

### Læringsmål der arbejdes med i faget denne uge

**Overordnede læringsmål fra studie ordningen:**

- **Viden:** ..
- **Færdigheder:** ..
- **Kompetencer:** ..

**Læringsmål den studerende kan bruge til selvvurdering**

- **Viden:** ..
- **Færdigheder:** ..
- **Kompetencer:** ..

## Afleveringer

- ...

## Skema

### Mandag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 08:15  | Introduktion til dagen |
| 11:30 | Fyraften               |

## Kommentarer

- ..
